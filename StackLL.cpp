#include <iostream>
#include "StackLL.h"
using namespace std;

class Stack::Node
{
	public:
		int data;
		Node* next;
};

Stack::~Stack()
{

	while(num_elements != 0)
	{
		pop();
	}

}

void Stack::push(int x)
{
	Node *newTop = new Node{x};
	
	if (frontPtr == NULL)
	{
	//	newTop -> data = x;
	//	newTop -> next = NULL;
		frontPtr = newTop;
		num_elements++;
	}
	else{
		//	newTop -> data = x;
			newTop -> next = frontPtr;
			frontPtr = newTop;
			num_elements++;
		}
}

void Stack::pop()
{
	if(frontPtr == NULL)
	{
		cout << "There is nothing to pop";
	}
	else{
			Node* old = frontPtr;
			frontPtr = frontPtr -> next;
			num_elements--;
			delete old;
		}
}

int Stack::size()
{
	cout << num_elements << endl;
	
	return num_elements;
}

int Stack::top()
{
	cout << frontPtr -> data;
	return frontPtr -> data;
}



